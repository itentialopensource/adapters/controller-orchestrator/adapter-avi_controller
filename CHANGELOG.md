
## 0.4.5 [10-15-2024]

* Changes made at 2024.10.14_20:41PM

See merge request itentialopensource/adapters/adapter-avi_controller!15

---

## 0.4.4 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-avi_controller!13

---

## 0.4.3 [08-14-2024]

* Changes made at 2024.08.14_18:57PM

See merge request itentialopensource/adapters/adapter-avi_controller!12

---

## 0.4.2 [08-07-2024]

* Changes made at 2024.08.06_20:09PM

See merge request itentialopensource/adapters/adapter-avi_controller!11

---

## 0.4.1 [08-06-2024]

* Changes made at 2024.08.06_12:54PM

See merge request itentialopensource/adapters/adapter-avi_controller!10

---

## 0.4.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-avi_controller!9

---

## 0.3.3 [03-28-2024]

* Changes made at 2024.03.28_13:42PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-avi_controller!8

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:59PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-avi_controller!7

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:28AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-avi_controller!6

---

## 0.3.0 [12-28-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-avi_controller!5

---

## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-avi_controller!4

---

## 0.1.5 [03-18-2021]

- Changes to authentication for POST calls -- configuration in token request and sampleProperties.
also needed to remove fake headers that were automatically built into the adapter.js

See merge request itentialopensource/adapters/controller-orchestrator/adapter-avi_controller!3

---

## 0.1.4 [03-17-2021]

- Remove the integer formats in the pronghorn.json as IAP is having issues with them since node does not support them.

See merge request itentialopensource/adapters/controller-orchestrator/adapter-avi_controller!2

---

## 0.1.3 [02-25-2021] & 0.1.2 [02-05-2021]

- Fix some issues with last migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-avi_controller!1

---

## 0.1.1 [01-28-2021]

- Initial Commit

See commit 111669d

---
