# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the AviController System. The API that was used to build the adapter for AviController is usually available in the report directory of this adapter. The adapter utilizes the AviController API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The AVI Controller adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AVI Controller. With this adapter you have the ability to perform operations such as:

- Get Virtual Service
- Create Virtual Service

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
