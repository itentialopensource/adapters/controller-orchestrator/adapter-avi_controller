
## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-avi_controller!4

---

## 0.1.5 [03-18-2021]

- Changes to authentication for POST calls -- configuration in token request and sampleProperties.
also needed to remove fake headers that were automatically built into the adapter.js

See merge request itentialopensource/adapters/controller-orchestrator/adapter-avi_controller!3

---

## 0.1.4 [03-17-2021]

- Remove the integer formats in the pronghorn.json as IAP is having issues with them since node does not support them.

See merge request itentialopensource/adapters/controller-orchestrator/adapter-avi_controller!2

---

## 0.1.3 [02-25-2021] & 0.1.2 [02-05-2021]

- Fix some issues with last migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-avi_controller!1

---

## 0.1.1 [01-28-2021]

- Initial Commit

See commit 111669d

---
