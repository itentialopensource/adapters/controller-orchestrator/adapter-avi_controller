## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for AVI Controller. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for AVI Controller.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the AVI Networks Controller. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceGET(name, refersTo, referredBy, fields, includeName, skipDefault, joinSubresources, cloudUuid, cloudRefName, callback)</td>
    <td style="padding:15px">Virtualservice_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualservicePOST(body, callback)</td>
    <td style="padding:15px">Virtualservice_POST</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceByUuidGET(name, uuid, fields, includeName, skipDefault, joinSubresources, callback)</td>
    <td style="padding:15px">VirtualserviceByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceByUuidPUT(name, uuid, body, callback)</td>
    <td style="padding:15px">VirtualserviceByUuid_PUT</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceByUuidPATCH(name, uuid, body, callback)</td>
    <td style="padding:15px">VirtualserviceByUuid_PATCH</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceByUuidDELETE(name, uuid, callback)</td>
    <td style="padding:15px">VirtualserviceByUuid_DELETE</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceScaleoutByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">VirtualserviceScaleoutByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/scaleout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceScaleinByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">VirtualserviceScaleinByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/scalein?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceMigrateByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">VirtualserviceMigrateByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/migrate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceSwitchoverByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">VirtualserviceSwitchoverByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/switchover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceClearByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">VirtualserviceClearByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceResyncByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">VirtualserviceResyncByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/resync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceRotatekeysByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">VirtualserviceRotatekeysByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/rotatekeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceApicplacementByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">VirtualserviceApicplacementByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/apicplacement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceRetryplacementByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">VirtualserviceRetryplacementByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/retryplacement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceRuntimeByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceRuntimeByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/runtime/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceRuntimeDetailByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceRuntimeDetailByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/runtime/detail/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceRuntimeInternalByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceRuntimeInternalByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/runtime/internal/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceUdpstatByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceUdpstatByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/udpstat/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceTcpstatByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceTcpstatByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/tcpstat/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceTrafficCloneStatsByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceTrafficCloneStatsByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/traffic_clone_stats/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceDosstatByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceDosstatByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/dosstat/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceConnectionsByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceConnectionsByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/connections/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceHttpconnectionsByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceHttpconnectionsByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/httpconnections/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceHttpconnectionsDetailGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceHttpconnectionsDetail_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/httpconnections/detail/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceHttpstatsByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceHttpstatsByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/httpstats/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceAuthstatsByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceAuthstatsByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/authstats/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceHttppolicysetByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceHttppolicysetByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/httppolicyset/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceHttppolicysetstatsByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceHttppolicysetstatsByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/httppolicysetstats/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceDnspolicystatsByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceDnspolicystatsByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/dnspolicystats/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceNetworksecuritypolicystatsGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceNetworksecuritypolicystats_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/networksecuritypolicystats/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceNetworksecuritypolicyDetailGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceNetworksecuritypolicyDetail_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/networksecuritypolicy/detail/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceCandidatesehostlistByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceCandidatesehostlistByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/candidatesehostlist/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualservicePlacementByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualservicePlacementByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/placement/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceKeyvalByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceKeyvalByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/keyval/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceKeyvalsummaryByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceKeyvalsummaryByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/keyvalsummary/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceSslsessioncacheByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceSslsessioncacheByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/sslsessioncache/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceCltrackByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceCltrackByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/cltrack/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceCltracksummaryByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceCltracksummaryByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/cltracksummary/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceClientByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceClientByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/client/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceClientsummaryByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceClientsummaryByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/clientsummary/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceDnstableByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceDnstableByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/dnstable/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceGslbservicedetailByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceGslbservicedetailByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/gslbservicedetail/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceGslbserviceinternalByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceGslbserviceinternalByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/gslbserviceinternal/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceGslbservicealgostatByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceGslbservicealgostatByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/gslbservicealgostat/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceGslbservicehmonstatByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceGslbservicehmonstatByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/gslbservicehmonstat/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceGeolocationinfoByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceGeolocationinfoByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/geolocationinfo/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceGeodbinternalByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceGeodbinternalByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/geodbinternal/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceGslbsiteinternalByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceGslbsiteinternalByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/gslbsiteinternal/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceUserdefineddatascriptcountersGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceUserdefineddatascriptcounters_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/userdefineddatascriptcounters/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceL4policysetstatsByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceL4policysetstatsByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/l4policysetstats/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceSescaleoutstatusByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceSescaleoutstatusByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/sescaleoutstatus/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceScaleoutstatusByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceScaleoutstatusByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/scaleoutstatus/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceScaleoutstatusDetailByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceScaleoutstatusDetailByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/scaleoutstatus/detail/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">virtualserviceSsopolicystatsByUuidGET(uuid, callback)</td>
    <td style="padding:15px">VirtualserviceSsopolicystatsByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/virtualservice/{pathv1}/ssopolicystats/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ssopolicyGET(name, refersTo, referredBy, fields, includeName, skipDefault, joinSubresources, callback)</td>
    <td style="padding:15px">Ssopolicy_GET</td>
    <td style="padding:15px">{base_path}/{version}/ssopolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ssopolicyPOST(body, callback)</td>
    <td style="padding:15px">Ssopolicy_POST</td>
    <td style="padding:15px">{base_path}/{version}/ssopolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ssopolicyByUuidGET(name, uuid, fields, includeName, skipDefault, joinSubresources, callback)</td>
    <td style="padding:15px">SsopolicyByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/ssopolicy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ssopolicyByUuidPUT(name, uuid, body, callback)</td>
    <td style="padding:15px">SsopolicyByUuid_PUT</td>
    <td style="padding:15px">{base_path}/{version}/ssopolicy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ssopolicyByUuidPATCH(name, uuid, body, callback)</td>
    <td style="padding:15px">SsopolicyByUuid_PATCH</td>
    <td style="padding:15px">{base_path}/{version}/ssopolicy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ssopolicyByUuidDELETE(name, uuid, callback)</td>
    <td style="padding:15px">SsopolicyByUuid_DELETE</td>
    <td style="padding:15px">{base_path}/{version}/ssopolicy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">controllerpropertiesGET(name, refersTo, referredBy, fields, includeName, skipDefault, joinSubresources, callback)</td>
    <td style="padding:15px">Controllerproperties_GET</td>
    <td style="padding:15px">{base_path}/{version}/controllerproperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">controllerpropertiesPUT(name, body, callback)</td>
    <td style="padding:15px">Controllerproperties_PUT</td>
    <td style="padding:15px">{base_path}/{version}/controllerproperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">controllerpropertiesPATCH(name, body, callback)</td>
    <td style="padding:15px">Controllerproperties_PATCH</td>
    <td style="padding:15px">{base_path}/{version}/controllerproperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkGET(name, refersTo, referredBy, fields, includeName, skipDefault, joinSubresources, cloudUuid, cloudRefName, callback)</td>
    <td style="padding:15px">Network_GET</td>
    <td style="padding:15px">{base_path}/{version}/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkPOST(body, callback)</td>
    <td style="padding:15px">Network_POST</td>
    <td style="padding:15px">{base_path}/{version}/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkByUuidGET(name, uuid, fields, includeName, skipDefault, joinSubresources, callback)</td>
    <td style="padding:15px">NetworkByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkByUuidPUT(name, uuid, body, callback)</td>
    <td style="padding:15px">NetworkByUuid_PUT</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkByUuidPATCH(name, uuid, body, callback)</td>
    <td style="padding:15px">NetworkByUuid_PATCH</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkByUuidDELETE(name, uuid, callback)</td>
    <td style="padding:15px">NetworkByUuid_DELETE</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolGET(name, refersTo, referredBy, fields, includeName, skipDefault, joinSubresources, cloudUuid, cloudRefName, callback)</td>
    <td style="padding:15px">Pool_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolPOST(body, callback)</td>
    <td style="padding:15px">Pool_POST</td>
    <td style="padding:15px">{base_path}/{version}/pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolByUuidGET(name, uuid, fields, includeName, skipDefault, joinSubresources, callback)</td>
    <td style="padding:15px">PoolByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolByUuidPUT(name, uuid, body, callback)</td>
    <td style="padding:15px">PoolByUuid_PUT</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolByUuidPATCH(name, uuid, body, callback)</td>
    <td style="padding:15px">PoolByUuid_PATCH</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolByUuidDELETE(name, uuid, callback)</td>
    <td style="padding:15px">PoolByUuid_DELETE</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolScaleoutByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">PoolScaleoutByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/scaleout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolScaleinByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">PoolScaleinByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/scalein?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolClearByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">PoolClearByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolRuntimeByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolRuntimeByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/runtime/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolRuntimeServerByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolRuntimeServerByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/runtime/server/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolRuntimeDetailByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolRuntimeDetailByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/runtime/detail/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolRuntimeServerDetailByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolRuntimeServerDetailByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/runtime/server/detail/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolRuntimeInternalByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolRuntimeInternalByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/runtime/internal/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolRuntimeServerInternalByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolRuntimeServerInternalByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/runtime/server/internal/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolRuntimeStatsClearByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">PoolRuntimeStatsClearByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/runtime/stats/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolRuntimeRequestQueueClearByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">PoolRuntimeRequestQueueClearByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/runtime/request_queue/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolRuntimeDebugByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolRuntimeDebugByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/runtime/debug/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolHmonByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolHmonByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/hmon/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolRuntimeServerHmonstatByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolRuntimeServerHmonstatByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/runtime/server/hmonstat/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolAlgoByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolAlgoByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/algo/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolPersistenceByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolPersistenceByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/persistence/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolConnpoolByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolConnpoolByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/connpool/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolConnpoolstatsClearByUuidPOST(uuid, body, callback)</td>
    <td style="padding:15px">PoolConnpoolstatsClearByUuid_POST</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/connpoolstats/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolHttpcacheByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolHttpcacheByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/httpcache/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolHttpcachestatsByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolHttpcachestatsByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/httpcachestats/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolHttpcachestatsDetailByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolHttpcachestatsDetailByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/httpcachestats/detail/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolVsByUuidGET(uuid, callback)</td>
    <td style="padding:15px">PoolVsByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/pool/{pathv1}/vs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolgroupGET(name, refersTo, referredBy, fields, includeName, skipDefault, joinSubresources, cloudUuid, cloudRefName, callback)</td>
    <td style="padding:15px">Poolgroup_GET</td>
    <td style="padding:15px">{base_path}/{version}/poolgroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolgroupPOST(body, callback)</td>
    <td style="padding:15px">Poolgroup_POST</td>
    <td style="padding:15px">{base_path}/{version}/poolgroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolgroupByUuidGET(name, uuid, fields, includeName, skipDefault, joinSubresources, callback)</td>
    <td style="padding:15px">PoolgroupByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/poolgroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolgroupByUuidPUT(name, uuid, body, callback)</td>
    <td style="padding:15px">PoolgroupByUuid_PUT</td>
    <td style="padding:15px">{base_path}/{version}/poolgroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolgroupByUuidPATCH(name, uuid, body, callback)</td>
    <td style="padding:15px">PoolgroupByUuid_PATCH</td>
    <td style="padding:15px">{base_path}/{version}/poolgroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">poolgroupByUuidDELETE(name, uuid, callback)</td>
    <td style="padding:15px">PoolgroupByUuid_DELETE</td>
    <td style="padding:15px">{base_path}/{version}/poolgroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vsvipGET(name, refersTo, referredBy, fields, includeName, skipDefault, joinSubresources, cloudUuid, cloudRefName, callback)</td>
    <td style="padding:15px">Vsvip_GET</td>
    <td style="padding:15px">{base_path}/{version}/vsvip?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vsvipPOST(body, callback)</td>
    <td style="padding:15px">Vsvip_POST</td>
    <td style="padding:15px">{base_path}/{version}/vsvip?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vsvipByUuidGET(name, uuid, fields, includeName, skipDefault, joinSubresources, callback)</td>
    <td style="padding:15px">VsvipByUuid_GET</td>
    <td style="padding:15px">{base_path}/{version}/vsvip/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vsvipByUuidPUT(name, uuid, body, callback)</td>
    <td style="padding:15px">VsvipByUuid_PUT</td>
    <td style="padding:15px">{base_path}/{version}/vsvip/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vsvipByUuidPATCH(name, uuid, body, callback)</td>
    <td style="padding:15px">VsvipByUuid_PATCH</td>
    <td style="padding:15px">{base_path}/{version}/vsvip/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vsvipByUuidDELETE(name, uuid, callback)</td>
    <td style="padding:15px">VsvipByUuid_DELETE</td>
    <td style="padding:15px">{base_path}/{version}/vsvip/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
