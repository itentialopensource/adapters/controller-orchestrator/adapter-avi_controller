# AVI Networks Controller

Vendor: AVI Networks
Homepage: https://avinetworks.com/

Product: Networks Controller
Product Page: https://avinetworks.com/software-load-balancer/

## Introduction
We classify AVI Controller into the Data Center and Network Services domains as it provides a solution for automated load balancing, security, and analytics across multi-cloud environments.

## Why Integrate
The AVI Controller adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AVI Controller. With this adapter you have the ability to perform operations such as:

- Get Virtual Service
- Create Virtual Service

## Additional Product Documentation
[AVI Controller API Doc](https://avinetworks.com/docs/22.1/api-guide/)