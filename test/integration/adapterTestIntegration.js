/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-avi_controller',
      type: 'AviController',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const AviController = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] AviController Adapter Test', () => {
  describe('AviController Class Tests', () => {
    const a = new AviController(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    let virtualserviceUuid = 'fakedata';
    const virtualserviceVirtualservicePOSTBodyParam = {
      name: 'string'
    };
    describe('#virtualservicePOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualservicePOST(virtualserviceVirtualservicePOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('ACTIVE_STANDBY_SE_1', data.response.active_standby_se_tag);
                assert.equal(true, data.response.advertise_down_vs);
                assert.equal(false, data.response.allow_invalid_client_cert);
              } else {
                runCommonAsserts(data, error);
              }
              virtualserviceUuid = data.response.uuid;
              saveMockData('Virtualservice', 'virtualservicePOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualserviceVirtualserviceApicplacementByUuidPOSTBodyParam = {
      graph: 'string',
      lifs: [
        {
          cifs: [
            {
              adapter: 'string',
              cif: 'string',
              mac_address: 'string',
              resources: [
                'string'
              ],
              se_uuid: 'string'
            }
          ],
          lif: 'string',
          lif_label: 'string',
          subnet: 'string'
        }
      ],
      network_rel: [
        {
          connector: 'string',
          rel_key: 'string',
          target_network: 'string'
        }
      ],
      tenant_name: 'string',
      vdev: 'string',
      vgrp: 'string'
    };
    describe('#virtualserviceApicplacementByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceApicplacementByUuidPOST(virtualserviceUuid, virtualserviceVirtualserviceApicplacementByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceApicplacementByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualserviceVirtualserviceClearByUuidPOSTBodyParam = {};
    describe('#virtualserviceClearByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceClearByUuidPOST(virtualserviceUuid, virtualserviceVirtualserviceClearByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceClearByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualserviceVirtualserviceMigrateByUuidPOSTBodyParam = {
      vip_id: 'string'
    };
    describe('#virtualserviceMigrateByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceMigrateByUuidPOST(virtualserviceUuid, virtualserviceVirtualserviceMigrateByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceMigrateByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualserviceVirtualserviceResyncByUuidPOSTBodyParam = {
      se_ref: [
        'string'
      ],
      uuid: 'string'
    };
    describe('#virtualserviceResyncByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceResyncByUuidPOST(virtualserviceUuid, virtualserviceVirtualserviceResyncByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceResyncByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualserviceVirtualserviceRetryplacementByUuidPOSTBodyParam = {
      vip_id: 'string'
    };
    describe('#virtualserviceRetryplacementByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceRetryplacementByUuidPOST(virtualserviceUuid, virtualserviceVirtualserviceRetryplacementByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceRetryplacementByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualserviceVirtualserviceRotatekeysByUuidPOSTBodyParam = {};
    describe('#virtualserviceRotatekeysByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceRotatekeysByUuidPOST(virtualserviceUuid, virtualserviceVirtualserviceRotatekeysByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceRotatekeysByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualserviceVirtualserviceScaleinByUuidPOSTBodyParam = {
      vip_id: 'string'
    };
    describe('#virtualserviceScaleinByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceScaleinByUuidPOST(virtualserviceUuid, virtualserviceVirtualserviceScaleinByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceScaleinByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualserviceVirtualserviceScaleoutByUuidPOSTBodyParam = {
      vip_id: 'string'
    };
    describe('#virtualserviceScaleoutByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceScaleoutByUuidPOST(virtualserviceUuid, virtualserviceVirtualserviceScaleoutByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceScaleoutByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualserviceVirtualserviceSwitchoverByUuidPOSTBodyParam = {
      vip_id: 'string'
    };
    describe('#virtualserviceSwitchoverByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceSwitchoverByUuidPOST(virtualserviceUuid, virtualserviceVirtualserviceSwitchoverByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceSwitchoverByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualserviceGET(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
                assert.equal('string', data.response.next);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualserviceVirtualserviceByUuidPUTBodyParam = {
      name: 'string'
    };
    describe('#virtualserviceByUuidPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualserviceByUuidPUT(null, virtualserviceUuid, virtualserviceVirtualserviceByUuidPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceByUuidPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualserviceVirtualserviceByUuidPATCHBodyParam = {
      name: 'string'
    };
    describe('#virtualserviceByUuidPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualserviceByUuidPATCH(null, virtualserviceUuid, virtualserviceVirtualserviceByUuidPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceByUuidPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceByUuidGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.virtualserviceByUuidGET(null, virtualserviceUuid, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('ACTIVE_STANDBY_SE_1', data.response.active_standby_se_tag);
                assert.equal(false, data.response.advertise_down_vs);
                assert.equal(false, data.response.allow_invalid_client_cert);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceAuthstatsByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceAuthstatsByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceAuthstatsByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceCandidatesehostlistByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceCandidatesehostlistByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceCandidatesehostlistByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceClientByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceClientByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceClientByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceClientsummaryByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceClientsummaryByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceClientsummaryByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceCltrackByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceCltrackByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceCltrackByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceCltracksummaryByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceCltracksummaryByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceCltracksummaryByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceConnectionsByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceConnectionsByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceConnectionsByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceDnspolicystatsByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceDnspolicystatsByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceDnspolicystatsByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceDnstableByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceDnstableByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceDnstableByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceDosstatByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceDosstatByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceDosstatByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGeodbinternalByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceGeodbinternalByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceGeodbinternalByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGeolocationinfoByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceGeolocationinfoByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceGeolocationinfoByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGslbservicealgostatByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceGslbservicealgostatByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceGslbservicealgostatByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGslbservicedetailByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceGslbservicedetailByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceGslbservicedetailByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGslbservicehmonstatByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceGslbservicehmonstatByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceGslbservicehmonstatByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGslbserviceinternalByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceGslbserviceinternalByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceGslbserviceinternalByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGslbsiteinternalByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceGslbsiteinternalByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceGslbsiteinternalByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceHttpconnectionsByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceHttpconnectionsByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceHttpconnectionsByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceHttpconnectionsDetailGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceHttpconnectionsDetailGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceHttpconnectionsDetailGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceHttppolicysetByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceHttppolicysetByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceHttppolicysetByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceHttppolicysetstatsByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceHttppolicysetstatsByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceHttppolicysetstatsByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceHttpstatsByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceHttpstatsByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceHttpstatsByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceKeyvalByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceKeyvalByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceKeyvalByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceKeyvalsummaryByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceKeyvalsummaryByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceKeyvalsummaryByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceL4policysetstatsByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceL4policysetstatsByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceL4policysetstatsByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceNetworksecuritypolicyDetailGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceNetworksecuritypolicyDetailGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceNetworksecuritypolicyDetailGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceNetworksecuritypolicystatsGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceNetworksecuritypolicystatsGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceNetworksecuritypolicystatsGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualservicePlacementByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualservicePlacementByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualservicePlacementByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceRuntimeByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceRuntimeByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceRuntimeByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceRuntimeDetailByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceRuntimeDetailByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceRuntimeDetailByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceRuntimeInternalByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceRuntimeInternalByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceRuntimeInternalByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceScaleoutstatusByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceScaleoutstatusByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceScaleoutstatusByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceScaleoutstatusDetailByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceScaleoutstatusDetailByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceScaleoutstatusDetailByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceSescaleoutstatusByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceSescaleoutstatusByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceSescaleoutstatusByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceSslsessioncacheByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceSslsessioncacheByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceSslsessioncacheByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceSsopolicystatsByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceSsopolicystatsByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceSsopolicystatsByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceTcpstatByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceTcpstatByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceTcpstatByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceTrafficCloneStatsByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceTrafficCloneStatsByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceTrafficCloneStatsByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceUdpstatByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceUdpstatByUuidGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceUdpstatByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceUserdefineddatascriptcountersGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceUserdefineddatascriptcountersGET(virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceUserdefineddatascriptcountersGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let ssopolicyUuid = 'fakedata';
    const ssopolicySsopolicyPOSTBodyParam = {
      authentication_policy: {},
      name: 'string',
      type: 'SSO_TYPE_SAML'
    };
    describe('#ssopolicyPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ssopolicyPOST(ssopolicySsopolicyPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.authentication_policy);
                assert.equal('object', typeof data.response.authorization_policy);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.tenant_ref);
                assert.equal('SSO_TYPE_SAML', data.response.type);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              ssopolicyUuid = data.response.uuid;
              saveMockData('Ssopolicy', 'ssopolicyPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ssopolicyGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ssopolicyGET(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
                assert.equal('string', data.response.next);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssopolicy', 'ssopolicyGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ssopolicySsopolicyByUuidPUTBodyParam = {
      authentication_policy: {},
      name: 'string',
      type: 'SSO_TYPE_SAML'
    };
    describe('#ssopolicyByUuidPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ssopolicyByUuidPUT(null, ssopolicyUuid, ssopolicySsopolicyByUuidPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssopolicy', 'ssopolicyByUuidPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ssopolicySsopolicyByUuidPATCHBodyParam = {
      authentication_policy: {},
      name: 'string',
      type: 'SSO_TYPE_SAML'
    };
    describe('#ssopolicyByUuidPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ssopolicyByUuidPATCH(null, ssopolicyUuid, ssopolicySsopolicyByUuidPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssopolicy', 'ssopolicyByUuidPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ssopolicyByUuidGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ssopolicyByUuidGET(null, ssopolicyUuid, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.authentication_policy);
                assert.equal('object', typeof data.response.authorization_policy);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.tenant_ref);
                assert.equal('SSO_TYPE_SAML', data.response.type);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssopolicy', 'ssopolicyByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceByUuidDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.virtualserviceByUuidDELETE(null, virtualserviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualservice', 'virtualserviceByUuidDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ssopolicyByUuidDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ssopolicyByUuidDELETE(null, ssopolicyUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssopolicy', 'ssopolicyByUuidDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#controllerpropertiesGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.controllerpropertiesGET(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.allow_admin_network_updates);
                assert.equal(true, data.response.allow_ip_forwarding);
                assert.equal(false, data.response.allow_unauthenticated_apis);
                assert.equal(true, data.response.allow_unauthenticated_nodes);
                assert.equal(15, data.response.api_idle_timeout);
                assert.equal(10000, data.response.api_perf_logging_threshold);
                assert.equal(true, data.response.appviewx_compat_mode);
                assert.equal(360, data.response.attach_ip_retry_interval);
                assert.equal(4, data.response.attach_ip_retry_limit);
                assert.equal(true, data.response.bm_use_ansible);
                assert.equal(60, data.response.cleanup_expired_authtoken_timeout_period);
                assert.equal(60, data.response.cleanup_sessions_timeout_period);
                assert.equal(true, data.response.cloud_reconcile);
                assert.equal(60, data.response.cluster_ip_gratuitous_arp_period);
                assert.equal(60, data.response.consistency_check_timeout_period);
                assert.equal(900, data.response.crashed_se_reboot);
                assert.equal(360, data.response.dead_se_detection_timer);
                assert.equal(60, data.response.default_minimum_api_timeout);
                assert.equal(60, data.response.dns_refresh_period);
                assert.equal(7, data.response.dummy);
                assert.equal(true, data.response.edit_system_limits);
                assert.equal(true, data.response.enable_api_sharding);
                assert.equal(true, data.response.enable_memory_balancer);
                assert.equal(120, data.response.fatal_error_lease_time);
                assert.equal(120, data.response.federated_datastore_cleanup_duration);
                assert.equal(1440, data.response.file_object_cleanup_period);
                assert.equal(1, data.response.max_dead_se_in_grp);
                assert.equal(4, data.response.max_pcap_per_tenant);
                assert.equal(1800, data.response.max_se_spawn_interval_delay);
                assert.equal(3, data.response.max_seq_attach_ip_failures);
                assert.equal(3, data.response.max_seq_vnic_failures);
                assert.equal(true, data.response.permission_scoped_shared_admin_networks);
                assert.equal(1, data.response.persistence_key_rotate_period);
                assert.equal(10, data.response.portal_request_burst_limit);
                assert.equal(4, data.response.portal_request_rate_limit);
                assert.equal('string', data.response.portal_token);
                assert.equal(1, data.response.process_locked_useraccounts_timeout_period);
                assert.equal(1440, data.response.process_pki_profile_timeout_period);
                assert.equal(180, data.response.query_host_fail);
                assert.equal('string', data.response.safenet_hsm_version);
                assert.equal(900, data.response.se_create_timeout);
                assert.equal(300, data.response.se_failover_attempt_interval);
                assert.equal('IMAGE', data.response.se_from_marketplace);
                assert.equal(172000, data.response.se_offline_del);
                assert.equal(300, data.response.se_spawn_retry_interval);
                assert.equal(120, data.response.se_vnic_cooldown);
                assert.equal(60, data.response.secure_channel_cleanup_timeout);
                assert.equal(60, data.response.secure_channel_controller_token_timeout);
                assert.equal(60, data.response.secure_channel_se_token_timeout);
                assert.equal(5, data.response.seupgrade_copy_pool_size);
                assert.equal(20, data.response.seupgrade_fabric_pool_size);
                assert.equal(360, data.response.seupgrade_segroup_min_dead_timeout);
                assert.equal(false, data.response.shared_ssl_certificates);
                assert.equal(true, Array.isArray(data.response.ssl_certificate_expiry_warning_days));
                assert.equal(300, data.response.unresponsive_se_reboot);
                assert.equal(5, data.response.upgrade_dns_ttl);
                assert.equal(1200, data.response.upgrade_fat_se_lease_time);
                assert.equal(600, data.response.upgrade_lease_time);
                assert.equal(3, data.response.upgrade_se_per_vs_scale_ops_txn_time);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
                assert.equal(180, data.response.vnic_op_fail_time);
                assert.equal(360, data.response.vs_apic_scaleout_timeout);
                assert.equal(60, data.response.vs_awaiting_se_timeout);
                assert.equal(360, data.response.vs_key_rotate_period);
                assert.equal(60, data.response.vs_scaleout_ready_check_interval);
                assert.equal(600, data.response.vs_se_attach_ip_fail);
                assert.equal(480, data.response.vs_se_bootup_fail);
                assert.equal(1500, data.response.vs_se_create_fail);
                assert.equal(60, data.response.vs_se_ping_fail);
                assert.equal(300, data.response.vs_se_vnic_fail);
                assert.equal(120, data.response.vs_se_vnic_ip_fail);
                assert.equal(480, data.response.warmstart_se_reconnect_wait_time);
                assert.equal(300, data.response.warmstart_vs_resync_wait_time);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Controllerproperties', 'controllerpropertiesGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controllerpropertiesControllerpropertiesPUTBodyParam = {
      _last_modified: 'string',
      allow_admin_network_updates: true,
      allow_ip_forwarding: false,
      allow_unauthenticated_apis: false,
      allow_unauthenticated_nodes: true,
      api_idle_timeout: 15,
      api_perf_logging_threshold: 10000,
      appviewx_compat_mode: true,
      attach_ip_retry_interval: 360,
      attach_ip_retry_limit: 4,
      bm_use_ansible: true,
      cleanup_expired_authtoken_timeout_period: 60,
      cleanup_sessions_timeout_period: 60,
      cloud_reconcile: true,
      cluster_ip_gratuitous_arp_period: 60,
      consistency_check_timeout_period: 60,
      crashed_se_reboot: 900,
      dead_se_detection_timer: 360,
      default_minimum_api_timeout: 60,
      dns_refresh_period: 60,
      dummy: 4,
      edit_system_limits: true,
      enable_api_sharding: true,
      enable_memory_balancer: true,
      fatal_error_lease_time: 120,
      federated_datastore_cleanup_duration: 120,
      file_object_cleanup_period: 1440,
      max_dead_se_in_grp: 1,
      max_pcap_per_tenant: 4,
      max_se_spawn_interval_delay: 1800,
      max_seq_attach_ip_failures: 3,
      max_seq_vnic_failures: 3,
      permission_scoped_shared_admin_networks: true,
      persistence_key_rotate_period: 1,
      portal_request_burst_limit: 6,
      portal_request_rate_limit: 5,
      portal_token: 'string',
      process_locked_useraccounts_timeout_period: 1,
      process_pki_profile_timeout_period: 1440,
      query_host_fail: 180,
      safenet_hsm_version: 'string',
      se_create_timeout: 900,
      se_failover_attempt_interval: 300,
      se_from_marketplace: 'IMAGE',
      se_offline_del: 172000,
      se_spawn_retry_interval: 300,
      se_vnic_cooldown: 120,
      secure_channel_cleanup_timeout: 60,
      secure_channel_controller_token_timeout: 60,
      secure_channel_se_token_timeout: 60,
      seupgrade_copy_pool_size: 5,
      seupgrade_fabric_pool_size: 20,
      seupgrade_segroup_min_dead_timeout: 360,
      shared_ssl_certificates: true,
      ssl_certificate_expiry_warning_days: [
        3
      ],
      unresponsive_se_reboot: 300,
      upgrade_dns_ttl: 5,
      upgrade_fat_se_lease_time: 1200,
      upgrade_lease_time: 600,
      upgrade_se_per_vs_scale_ops_txn_time: 3,
      url: 'string',
      uuid: 'string',
      vnic_op_fail_time: 180,
      vs_apic_scaleout_timeout: 360,
      vs_awaiting_se_timeout: 60,
      vs_key_rotate_period: 360,
      vs_scaleout_ready_check_interval: 60,
      vs_se_attach_ip_fail: 600,
      vs_se_bootup_fail: 480,
      vs_se_create_fail: 1500,
      vs_se_ping_fail: 60,
      vs_se_vnic_fail: 300,
      vs_se_vnic_ip_fail: 120,
      warmstart_se_reconnect_wait_time: 480,
      warmstart_vs_resync_wait_time: 300
    };
    describe('#controllerpropertiesPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.controllerpropertiesPUT(null, controllerpropertiesControllerpropertiesPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Controllerproperties', 'controllerpropertiesPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controllerpropertiesControllerpropertiesPATCHBodyParam = {
      _last_modified: 'string',
      allow_admin_network_updates: false,
      allow_ip_forwarding: false,
      allow_unauthenticated_apis: true,
      allow_unauthenticated_nodes: true,
      api_idle_timeout: 15,
      api_perf_logging_threshold: 10000,
      appviewx_compat_mode: true,
      attach_ip_retry_interval: 360,
      attach_ip_retry_limit: 4,
      bm_use_ansible: true,
      cleanup_expired_authtoken_timeout_period: 60,
      cleanup_sessions_timeout_period: 60,
      cloud_reconcile: true,
      cluster_ip_gratuitous_arp_period: 60,
      consistency_check_timeout_period: 60,
      crashed_se_reboot: 900,
      dead_se_detection_timer: 360,
      default_minimum_api_timeout: 60,
      dns_refresh_period: 60,
      dummy: 6,
      edit_system_limits: false,
      enable_api_sharding: true,
      enable_memory_balancer: true,
      fatal_error_lease_time: 120,
      federated_datastore_cleanup_duration: 120,
      file_object_cleanup_period: 1440,
      max_dead_se_in_grp: 1,
      max_pcap_per_tenant: 4,
      max_se_spawn_interval_delay: 1800,
      max_seq_attach_ip_failures: 3,
      max_seq_vnic_failures: 3,
      permission_scoped_shared_admin_networks: true,
      persistence_key_rotate_period: 8,
      portal_request_burst_limit: 7,
      portal_request_rate_limit: 5,
      portal_token: 'string',
      process_locked_useraccounts_timeout_period: 1,
      process_pki_profile_timeout_period: 1440,
      query_host_fail: 180,
      safenet_hsm_version: 'string',
      se_create_timeout: 900,
      se_failover_attempt_interval: 300,
      se_from_marketplace: 'IMAGE',
      se_offline_del: 172000,
      se_spawn_retry_interval: 300,
      se_vnic_cooldown: 120,
      secure_channel_cleanup_timeout: 60,
      secure_channel_controller_token_timeout: 60,
      secure_channel_se_token_timeout: 60,
      seupgrade_copy_pool_size: 5,
      seupgrade_fabric_pool_size: 20,
      seupgrade_segroup_min_dead_timeout: 360,
      shared_ssl_certificates: true,
      ssl_certificate_expiry_warning_days: [
        1
      ],
      unresponsive_se_reboot: 300,
      upgrade_dns_ttl: 5,
      upgrade_fat_se_lease_time: 1200,
      upgrade_lease_time: 600,
      upgrade_se_per_vs_scale_ops_txn_time: 3,
      url: 'string',
      uuid: 'string',
      vnic_op_fail_time: 180,
      vs_apic_scaleout_timeout: 360,
      vs_awaiting_se_timeout: 60,
      vs_key_rotate_period: 360,
      vs_scaleout_ready_check_interval: 60,
      vs_se_attach_ip_fail: 600,
      vs_se_bootup_fail: 480,
      vs_se_create_fail: 1500,
      vs_se_ping_fail: 60,
      vs_se_vnic_fail: 300,
      vs_se_vnic_ip_fail: 120,
      warmstart_se_reconnect_wait_time: 480,
      warmstart_vs_resync_wait_time: 300
    };
    describe('#controllerpropertiesPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.controllerpropertiesPATCH(null, controllerpropertiesControllerpropertiesPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Controllerproperties', 'controllerpropertiesPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkGET(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
                assert.equal('string', data.response.next);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'networkGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkNetworkPOSTBodyParam = {
      name: 'string'
    };
    describe('#networkPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkPOST(networkNetworkPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.attrs));
                assert.equal('string', data.response.cloud_ref);
                assert.equal(true, Array.isArray(data.response.configured_subnets));
                assert.equal(true, data.response.dhcp_enabled);
                assert.equal(true, data.response.exclude_discovered_subnets);
                assert.equal(true, data.response.ip6_autocfg_enabled);
                assert.equal(true, Array.isArray(data.response.labels));
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.synced_from_se);
                assert.equal('string', data.response.tenant_ref);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
                assert.equal(true, data.response.vcenter_dvs);
                assert.equal('string', data.response.vimgrnw_ref);
                assert.equal('string', data.response.vrf_context_ref);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'networkPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkByUuidGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkByUuidGET(null, 'fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.attrs));
                assert.equal('string', data.response.cloud_ref);
                assert.equal(true, Array.isArray(data.response.configured_subnets));
                assert.equal(true, data.response.dhcp_enabled);
                assert.equal(false, data.response.exclude_discovered_subnets);
                assert.equal(true, data.response.ip6_autocfg_enabled);
                assert.equal(true, Array.isArray(data.response.labels));
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.synced_from_se);
                assert.equal('string', data.response.tenant_ref);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
                assert.equal(true, data.response.vcenter_dvs);
                assert.equal('string', data.response.vimgrnw_ref);
                assert.equal('string', data.response.vrf_context_ref);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'networkByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkNetworkByUuidPUTBodyParam = {
      name: 'string'
    };
    describe('#networkByUuidPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkByUuidPUT(null, 'fakedata', networkNetworkByUuidPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'networkByUuidPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkNetworkByUuidPATCHBodyParam = {
      name: 'string'
    };
    describe('#networkByUuidPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.networkByUuidPATCH(null, 'fakedata', networkNetworkByUuidPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'networkByUuidPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkByUuidDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.networkByUuidDELETE(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'networkByUuidDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.poolGET(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
                assert.equal('string', data.response.next);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const poolPoolPOSTBodyParam = {
      name: 'string'
    };
    describe('#poolPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.poolPOST(poolPoolPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.a_pool);
                assert.equal('object', typeof data.response.ab_pool);
                assert.equal(6, data.response.ab_priority);
                assert.equal('object', typeof data.response.analytics_policy);
                assert.equal('string', data.response.analytics_profile_ref);
                assert.equal('string', data.response.apic_epg_name);
                assert.equal('string', data.response.application_persistence_profile_ref);
                assert.equal('string', data.response.autoscale_launch_config_ref);
                assert.equal(true, Array.isArray(data.response.autoscale_networks));
                assert.equal('string', data.response.autoscale_policy_ref);
                assert.equal(true, data.response.capacity_estimation);
                assert.equal(5, data.response.capacity_estimation_ttfb_thresh);
                assert.equal('string', data.response.cloud_config_cksum);
                assert.equal('string', data.response.cloud_ref);
                assert.equal('object', typeof data.response.conn_pool_properties);
                assert.equal(10, data.response.connection_ramp_duration);
                assert.equal('string', data.response.created_by);
                assert.equal(80, data.response.default_server_port);
                assert.equal(true, data.response.delete_server_on_dns_refresh);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.domain_name));
                assert.equal(true, data.response.east_west);
                assert.equal(false, data.response.enable_http2);
                assert.equal(true, data.response.enabled);
                assert.equal(true, Array.isArray(data.response.external_autoscale_groups));
                assert.equal('object', typeof data.response.fail_action);
                assert.equal(10, data.response.fewest_tasks_feedback_delay);
                assert.equal(1, data.response.graceful_disable_timeout);
                assert.equal(false, data.response.gslb_sp_enabled);
                assert.equal(true, Array.isArray(data.response.health_monitor_refs));
                assert.equal(false, data.response.host_check_enabled);
                assert.equal(false, data.response.ignore_server_port);
                assert.equal(true, data.response.inline_health_monitor);
                assert.equal('string', data.response.ipaddrgroup_ref);
                assert.equal('LB_ALGORITHM_LEAST_CONNECTIONS', data.response.lb_algorithm);
                assert.equal('string', data.response.lb_algorithm_consistent_hash_hdr);
                assert.equal(2, data.response.lb_algorithm_core_nonaffinity);
                assert.equal('LB_ALGORITHM_CONSISTENT_HASH_SOURCE_IP_ADDRESS', data.response.lb_algorithm_hash);
                assert.equal(false, data.response.lookup_server_by_name);
                assert.equal(9, data.response.max_concurrent_connections_per_server);
                assert.equal('object', typeof data.response.max_conn_rate_per_server);
                assert.equal(1, data.response.min_health_monitors_up);
                assert.equal(8, data.response.min_servers_up);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal(true, Array.isArray(data.response.nsx_securitygroup));
                assert.equal('string', data.response.pki_profile_ref);
                assert.equal(true, Array.isArray(data.response.placement_networks));
                assert.equal('string', data.response.prst_hdr_name);
                assert.equal(128, data.response.request_queue_depth);
                assert.equal(true, data.response.request_queue_enabled);
                assert.equal(false, data.response.rewrite_host_header_to_server_name);
                assert.equal(false, data.response.rewrite_host_header_to_sni);
                assert.equal(true, data.response.routing_pool);
                assert.equal(true, data.response.server_auto_scale);
                assert.equal(5, data.response.server_count);
                assert.equal('string', data.response.server_name);
                assert.equal('object', typeof data.response.server_reselect);
                assert.equal(3, data.response.server_timeout);
                assert.equal(true, Array.isArray(data.response.servers));
                assert.equal('string', data.response.service_metadata);
                assert.equal(true, data.response.sni_enabled);
                assert.equal('string', data.response.ssl_key_and_certificate_ref);
                assert.equal('string', data.response.ssl_profile_ref);
                assert.equal('string', data.response.tenant_ref);
                assert.equal('string', data.response.tier1_lr);
                assert.equal('string', data.response.url);
                assert.equal(false, data.response.use_service_port);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.vrf_ref);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolByUuidGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.poolByUuidGET(null, 'fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.a_pool);
                assert.equal('object', typeof data.response.ab_pool);
                assert.equal(5, data.response.ab_priority);
                assert.equal('object', typeof data.response.analytics_policy);
                assert.equal('string', data.response.analytics_profile_ref);
                assert.equal('string', data.response.apic_epg_name);
                assert.equal('string', data.response.application_persistence_profile_ref);
                assert.equal('string', data.response.autoscale_launch_config_ref);
                assert.equal(true, Array.isArray(data.response.autoscale_networks));
                assert.equal('string', data.response.autoscale_policy_ref);
                assert.equal(true, data.response.capacity_estimation);
                assert.equal(2, data.response.capacity_estimation_ttfb_thresh);
                assert.equal('string', data.response.cloud_config_cksum);
                assert.equal('string', data.response.cloud_ref);
                assert.equal('object', typeof data.response.conn_pool_properties);
                assert.equal(10, data.response.connection_ramp_duration);
                assert.equal('string', data.response.created_by);
                assert.equal(80, data.response.default_server_port);
                assert.equal(true, data.response.delete_server_on_dns_refresh);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.domain_name));
                assert.equal(false, data.response.east_west);
                assert.equal(false, data.response.enable_http2);
                assert.equal(true, data.response.enabled);
                assert.equal(true, Array.isArray(data.response.external_autoscale_groups));
                assert.equal('object', typeof data.response.fail_action);
                assert.equal(10, data.response.fewest_tasks_feedback_delay);
                assert.equal(1, data.response.graceful_disable_timeout);
                assert.equal(false, data.response.gslb_sp_enabled);
                assert.equal(true, Array.isArray(data.response.health_monitor_refs));
                assert.equal(true, data.response.host_check_enabled);
                assert.equal(true, data.response.ignore_server_port);
                assert.equal(true, data.response.inline_health_monitor);
                assert.equal('string', data.response.ipaddrgroup_ref);
                assert.equal('LB_ALGORITHM_LEAST_CONNECTIONS', data.response.lb_algorithm);
                assert.equal('string', data.response.lb_algorithm_consistent_hash_hdr);
                assert.equal(2, data.response.lb_algorithm_core_nonaffinity);
                assert.equal('LB_ALGORITHM_CONSISTENT_HASH_SOURCE_IP_ADDRESS', data.response.lb_algorithm_hash);
                assert.equal(false, data.response.lookup_server_by_name);
                assert.equal(2, data.response.max_concurrent_connections_per_server);
                assert.equal('object', typeof data.response.max_conn_rate_per_server);
                assert.equal(7, data.response.min_health_monitors_up);
                assert.equal(2, data.response.min_servers_up);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal(true, Array.isArray(data.response.nsx_securitygroup));
                assert.equal('string', data.response.pki_profile_ref);
                assert.equal(true, Array.isArray(data.response.placement_networks));
                assert.equal('string', data.response.prst_hdr_name);
                assert.equal(128, data.response.request_queue_depth);
                assert.equal(true, data.response.request_queue_enabled);
                assert.equal(false, data.response.rewrite_host_header_to_server_name);
                assert.equal(false, data.response.rewrite_host_header_to_sni);
                assert.equal(true, data.response.routing_pool);
                assert.equal(true, data.response.server_auto_scale);
                assert.equal(8, data.response.server_count);
                assert.equal('string', data.response.server_name);
                assert.equal('object', typeof data.response.server_reselect);
                assert.equal(7, data.response.server_timeout);
                assert.equal(true, Array.isArray(data.response.servers));
                assert.equal('string', data.response.service_metadata);
                assert.equal(true, data.response.sni_enabled);
                assert.equal('string', data.response.ssl_key_and_certificate_ref);
                assert.equal('string', data.response.ssl_profile_ref);
                assert.equal('string', data.response.tenant_ref);
                assert.equal('string', data.response.tier1_lr);
                assert.equal('string', data.response.url);
                assert.equal(false, data.response.use_service_port);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.vrf_ref);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const poolPoolByUuidPUTBodyParam = {
      name: 'string'
    };
    describe('#poolByUuidPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.poolByUuidPUT(null, 'fakedata', poolPoolByUuidPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolByUuidPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const poolPoolByUuidPATCHBodyParam = {
      name: 'string'
    };
    describe('#poolByUuidPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.poolByUuidPATCH(null, 'fakedata', poolPoolByUuidPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolByUuidPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolByUuidDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolByUuidDELETE(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolByUuidDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const poolPoolScaleoutByUuidPOSTBodyParam = {
      reason: 'string',
      uuid: 'string'
    };
    describe('#poolScaleoutByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolScaleoutByUuidPOST('fakedata', poolPoolScaleoutByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolScaleoutByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const poolPoolScaleinByUuidPOSTBodyParam = {
      reason: 'string',
      servers: [
        {
          external_uuid: 'string',
          ip: {
            addr: 'string',
            type: 'string'
          },
          port: 10
        }
      ],
      uuid: 'string'
    };
    describe('#poolScaleinByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolScaleinByUuidPOST('fakedata', poolPoolScaleinByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolScaleinByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const poolPoolClearByUuidPOSTBodyParam = {};
    describe('#poolClearByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolClearByUuidPOST('fakedata', poolPoolClearByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolClearByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolRuntimeByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolRuntimeByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeServerByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolRuntimeServerByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolRuntimeServerByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeDetailByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolRuntimeDetailByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolRuntimeDetailByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeServerDetailByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolRuntimeServerDetailByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolRuntimeServerDetailByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeInternalByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolRuntimeInternalByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolRuntimeInternalByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeServerInternalByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolRuntimeServerInternalByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolRuntimeServerInternalByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const poolPoolRuntimeStatsClearByUuidPOSTBodyParam = {};
    describe('#poolRuntimeStatsClearByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolRuntimeStatsClearByUuidPOST('fakedata', poolPoolRuntimeStatsClearByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolRuntimeStatsClearByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const poolPoolRuntimeRequestQueueClearByUuidPOSTBodyParam = {};
    describe('#poolRuntimeRequestQueueClearByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolRuntimeRequestQueueClearByUuidPOST('fakedata', poolPoolRuntimeRequestQueueClearByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolRuntimeRequestQueueClearByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeDebugByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolRuntimeDebugByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolRuntimeDebugByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolHmonByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolHmonByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolHmonByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeServerHmonstatByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolRuntimeServerHmonstatByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolRuntimeServerHmonstatByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolAlgoByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolAlgoByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolAlgoByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolPersistenceByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolPersistenceByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolPersistenceByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolConnpoolByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolConnpoolByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolConnpoolByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const poolPoolConnpoolstatsClearByUuidPOSTBodyParam = {};
    describe('#poolConnpoolstatsClearByUuidPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolConnpoolstatsClearByUuidPOST('fakedata', poolPoolConnpoolstatsClearByUuidPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolConnpoolstatsClearByUuidPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolHttpcacheByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolHttpcacheByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolHttpcacheByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolHttpcachestatsByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolHttpcachestatsByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolHttpcachestatsByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolHttpcachestatsDetailByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolHttpcachestatsDetailByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolHttpcachestatsDetailByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolVsByUuidGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolVsByUuidGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pool', 'poolVsByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolgroupGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.poolgroupGET(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
                assert.equal('string', data.response.next);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Poolgroup', 'poolgroupGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const poolgroupPoolgroupPOSTBodyParam = {
      name: 'string'
    };
    describe('#poolgroupPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.poolgroupPOST(poolgroupPoolgroupPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.cloud_config_cksum);
                assert.equal('string', data.response.cloud_ref);
                assert.equal('string', data.response.created_by);
                assert.equal('string', data.response.deployment_policy_ref);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.enable_http2);
                assert.equal('object', typeof data.response.fail_action);
                assert.equal(false, data.response.implicit_priority_labels);
                assert.equal(true, Array.isArray(data.response.members));
                assert.equal(3, data.response.min_servers);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.priority_labels_ref);
                assert.equal('string', data.response.service_metadata);
                assert.equal('string', data.response.tenant_ref);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Poolgroup', 'poolgroupPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolgroupByUuidGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.poolgroupByUuidGET(null, 'fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.cloud_config_cksum);
                assert.equal('string', data.response.cloud_ref);
                assert.equal('string', data.response.created_by);
                assert.equal('string', data.response.deployment_policy_ref);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.enable_http2);
                assert.equal('object', typeof data.response.fail_action);
                assert.equal(false, data.response.implicit_priority_labels);
                assert.equal(true, Array.isArray(data.response.members));
                assert.equal(2, data.response.min_servers);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.priority_labels_ref);
                assert.equal('string', data.response.service_metadata);
                assert.equal('string', data.response.tenant_ref);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Poolgroup', 'poolgroupByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const poolgroupPoolgroupByUuidPUTBodyParam = {
      name: 'string'
    };
    describe('#poolgroupByUuidPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.poolgroupByUuidPUT(null, 'fakedata', poolgroupPoolgroupByUuidPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Poolgroup', 'poolgroupByUuidPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const poolgroupPoolgroupByUuidPATCHBodyParam = {
      name: 'string'
    };
    describe('#poolgroupByUuidPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.poolgroupByUuidPATCH(null, 'fakedata', poolgroupPoolgroupByUuidPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Poolgroup', 'poolgroupByUuidPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolgroupByUuidDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.poolgroupByUuidDELETE(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Poolgroup', 'poolgroupByUuidDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vsvipGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vsvipGET(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
                assert.equal('string', data.response.next);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vsvip', 'vsvipGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vsvipVsvipPOSTBodyParam = {
      name: 'string'
    };
    describe('#vsvipPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vsvipPOST(vsvipVsvipPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.cloud_ref);
                assert.equal(true, Array.isArray(data.response.dns_info));
                assert.equal(false, data.response.east_west_placement);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.tenant_ref);
                assert.equal('string', data.response.tier1_lr);
                assert.equal('string', data.response.url);
                assert.equal(false, data.response.use_standard_alb);
                assert.equal('string', data.response.uuid);
                assert.equal(true, Array.isArray(data.response.vip));
                assert.equal('string', data.response.vrf_context_ref);
                assert.equal('string', data.response.vsvip_cloud_config_cksum);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vsvip', 'vsvipPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vsvipByUuidGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vsvipByUuidGET(null, 'fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.cloud_ref);
                assert.equal(true, Array.isArray(data.response.dns_info));
                assert.equal(false, data.response.east_west_placement);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.tenant_ref);
                assert.equal('string', data.response.tier1_lr);
                assert.equal('string', data.response.url);
                assert.equal(false, data.response.use_standard_alb);
                assert.equal('string', data.response.uuid);
                assert.equal(true, Array.isArray(data.response.vip));
                assert.equal('string', data.response.vrf_context_ref);
                assert.equal('string', data.response.vsvip_cloud_config_cksum);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vsvip', 'vsvipByUuidGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vsvipVsvipByUuidPUTBodyParam = {
      name: 'string'
    };
    describe('#vsvipByUuidPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vsvipByUuidPUT(null, 'fakedata', vsvipVsvipByUuidPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vsvip', 'vsvipByUuidPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vsvipVsvipByUuidPATCHBodyParam = {
      name: 'string'
    };
    describe('#vsvipByUuidPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vsvipByUuidPATCH(null, 'fakedata', vsvipVsvipByUuidPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vsvip', 'vsvipByUuidPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vsvipByUuidDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.vsvipByUuidDELETE(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-avi_controller-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vsvip', 'vsvipByUuidDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
