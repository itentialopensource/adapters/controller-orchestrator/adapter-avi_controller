/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-avi_controller',
      type: 'AviController',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const AviController = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] AviController Adapter Test', () => {
  describe('AviController Class Tests', () => {
    const a = new AviController(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('avi_controller'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('avi_controller'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('AviController', pronghornDotJson.export);
          assert.equal('AviController', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-avi_controller', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('avi_controller'));
          assert.equal('AviController', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-avi_controller', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-avi_controller', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#virtualserviceGET - errors', () => {
      it('should have a virtualserviceGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualservicePOST - errors', () => {
      it('should have a virtualservicePOST function', (done) => {
        try {
          assert.equal(true, typeof a.virtualservicePOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.virtualservicePOST(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualservicePOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceByUuidGET - errors', () => {
      it('should have a virtualserviceByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceByUuidGET('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceByUuidPUT - errors', () => {
      it('should have a virtualserviceByUuidPUT function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceByUuidPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceByUuidPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceByUuidPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.virtualserviceByUuidPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceByUuidPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceByUuidPATCH - errors', () => {
      it('should have a virtualserviceByUuidPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceByUuidPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceByUuidPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceByUuidPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.virtualserviceByUuidPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceByUuidPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceByUuidDELETE - errors', () => {
      it('should have a virtualserviceByUuidDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceByUuidDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceByUuidDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceByUuidDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceScaleoutByUuidPOST - errors', () => {
      it('should have a virtualserviceScaleoutByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceScaleoutByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceScaleoutByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceScaleoutByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.virtualserviceScaleoutByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceScaleoutByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceScaleinByUuidPOST - errors', () => {
      it('should have a virtualserviceScaleinByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceScaleinByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceScaleinByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceScaleinByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.virtualserviceScaleinByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceScaleinByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceMigrateByUuidPOST - errors', () => {
      it('should have a virtualserviceMigrateByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceMigrateByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceMigrateByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceMigrateByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.virtualserviceMigrateByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceMigrateByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceSwitchoverByUuidPOST - errors', () => {
      it('should have a virtualserviceSwitchoverByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceSwitchoverByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceSwitchoverByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceSwitchoverByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.virtualserviceSwitchoverByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceSwitchoverByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceClearByUuidPOST - errors', () => {
      it('should have a virtualserviceClearByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceClearByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceClearByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceClearByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.virtualserviceClearByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceClearByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceResyncByUuidPOST - errors', () => {
      it('should have a virtualserviceResyncByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceResyncByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceResyncByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceResyncByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.virtualserviceResyncByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceResyncByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceRotatekeysByUuidPOST - errors', () => {
      it('should have a virtualserviceRotatekeysByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceRotatekeysByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceRotatekeysByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceRotatekeysByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.virtualserviceRotatekeysByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceRotatekeysByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceApicplacementByUuidPOST - errors', () => {
      it('should have a virtualserviceApicplacementByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceApicplacementByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceApicplacementByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceApicplacementByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.virtualserviceApicplacementByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceApicplacementByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceRetryplacementByUuidPOST - errors', () => {
      it('should have a virtualserviceRetryplacementByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceRetryplacementByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceRetryplacementByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceRetryplacementByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.virtualserviceRetryplacementByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceRetryplacementByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceRuntimeByUuidGET - errors', () => {
      it('should have a virtualserviceRuntimeByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceRuntimeByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceRuntimeByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceRuntimeByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceRuntimeDetailByUuidGET - errors', () => {
      it('should have a virtualserviceRuntimeDetailByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceRuntimeDetailByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceRuntimeDetailByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceRuntimeDetailByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceRuntimeInternalByUuidGET - errors', () => {
      it('should have a virtualserviceRuntimeInternalByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceRuntimeInternalByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceRuntimeInternalByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceRuntimeInternalByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceUdpstatByUuidGET - errors', () => {
      it('should have a virtualserviceUdpstatByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceUdpstatByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceUdpstatByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceUdpstatByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceTcpstatByUuidGET - errors', () => {
      it('should have a virtualserviceTcpstatByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceTcpstatByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceTcpstatByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceTcpstatByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceTrafficCloneStatsByUuidGET - errors', () => {
      it('should have a virtualserviceTrafficCloneStatsByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceTrafficCloneStatsByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceTrafficCloneStatsByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceTrafficCloneStatsByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceDosstatByUuidGET - errors', () => {
      it('should have a virtualserviceDosstatByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceDosstatByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceDosstatByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceDosstatByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceConnectionsByUuidGET - errors', () => {
      it('should have a virtualserviceConnectionsByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceConnectionsByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceConnectionsByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceConnectionsByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceHttpconnectionsByUuidGET - errors', () => {
      it('should have a virtualserviceHttpconnectionsByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceHttpconnectionsByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceHttpconnectionsByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceHttpconnectionsByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceHttpconnectionsDetailGET - errors', () => {
      it('should have a virtualserviceHttpconnectionsDetailGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceHttpconnectionsDetailGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceHttpconnectionsDetailGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceHttpconnectionsDetailGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceHttpstatsByUuidGET - errors', () => {
      it('should have a virtualserviceHttpstatsByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceHttpstatsByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceHttpstatsByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceHttpstatsByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceAuthstatsByUuidGET - errors', () => {
      it('should have a virtualserviceAuthstatsByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceAuthstatsByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceAuthstatsByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceAuthstatsByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceHttppolicysetByUuidGET - errors', () => {
      it('should have a virtualserviceHttppolicysetByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceHttppolicysetByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceHttppolicysetByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceHttppolicysetByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceHttppolicysetstatsByUuidGET - errors', () => {
      it('should have a virtualserviceHttppolicysetstatsByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceHttppolicysetstatsByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceHttppolicysetstatsByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceHttppolicysetstatsByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceDnspolicystatsByUuidGET - errors', () => {
      it('should have a virtualserviceDnspolicystatsByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceDnspolicystatsByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceDnspolicystatsByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceDnspolicystatsByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceNetworksecuritypolicystatsGET - errors', () => {
      it('should have a virtualserviceNetworksecuritypolicystatsGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceNetworksecuritypolicystatsGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceNetworksecuritypolicystatsGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceNetworksecuritypolicystatsGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceNetworksecuritypolicyDetailGET - errors', () => {
      it('should have a virtualserviceNetworksecuritypolicyDetailGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceNetworksecuritypolicyDetailGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceNetworksecuritypolicyDetailGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceNetworksecuritypolicyDetailGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceCandidatesehostlistByUuidGET - errors', () => {
      it('should have a virtualserviceCandidatesehostlistByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceCandidatesehostlistByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceCandidatesehostlistByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceCandidatesehostlistByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualservicePlacementByUuidGET - errors', () => {
      it('should have a virtualservicePlacementByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualservicePlacementByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualservicePlacementByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualservicePlacementByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceKeyvalByUuidGET - errors', () => {
      it('should have a virtualserviceKeyvalByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceKeyvalByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceKeyvalByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceKeyvalByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceKeyvalsummaryByUuidGET - errors', () => {
      it('should have a virtualserviceKeyvalsummaryByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceKeyvalsummaryByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceKeyvalsummaryByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceKeyvalsummaryByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceSslsessioncacheByUuidGET - errors', () => {
      it('should have a virtualserviceSslsessioncacheByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceSslsessioncacheByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceSslsessioncacheByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceSslsessioncacheByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceCltrackByUuidGET - errors', () => {
      it('should have a virtualserviceCltrackByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceCltrackByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceCltrackByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceCltrackByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceCltracksummaryByUuidGET - errors', () => {
      it('should have a virtualserviceCltracksummaryByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceCltracksummaryByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceCltracksummaryByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceCltracksummaryByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceClientByUuidGET - errors', () => {
      it('should have a virtualserviceClientByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceClientByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceClientByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceClientByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceClientsummaryByUuidGET - errors', () => {
      it('should have a virtualserviceClientsummaryByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceClientsummaryByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceClientsummaryByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceClientsummaryByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceDnstableByUuidGET - errors', () => {
      it('should have a virtualserviceDnstableByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceDnstableByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceDnstableByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceDnstableByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGslbservicedetailByUuidGET - errors', () => {
      it('should have a virtualserviceGslbservicedetailByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceGslbservicedetailByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceGslbservicedetailByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceGslbservicedetailByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGslbserviceinternalByUuidGET - errors', () => {
      it('should have a virtualserviceGslbserviceinternalByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceGslbserviceinternalByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceGslbserviceinternalByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceGslbserviceinternalByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGslbservicealgostatByUuidGET - errors', () => {
      it('should have a virtualserviceGslbservicealgostatByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceGslbservicealgostatByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceGslbservicealgostatByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceGslbservicealgostatByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGslbservicehmonstatByUuidGET - errors', () => {
      it('should have a virtualserviceGslbservicehmonstatByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceGslbservicehmonstatByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceGslbservicehmonstatByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceGslbservicehmonstatByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGeolocationinfoByUuidGET - errors', () => {
      it('should have a virtualserviceGeolocationinfoByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceGeolocationinfoByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceGeolocationinfoByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceGeolocationinfoByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGeodbinternalByUuidGET - errors', () => {
      it('should have a virtualserviceGeodbinternalByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceGeodbinternalByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceGeodbinternalByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceGeodbinternalByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceGslbsiteinternalByUuidGET - errors', () => {
      it('should have a virtualserviceGslbsiteinternalByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceGslbsiteinternalByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceGslbsiteinternalByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceGslbsiteinternalByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceUserdefineddatascriptcountersGET - errors', () => {
      it('should have a virtualserviceUserdefineddatascriptcountersGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceUserdefineddatascriptcountersGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceUserdefineddatascriptcountersGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceUserdefineddatascriptcountersGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceL4policysetstatsByUuidGET - errors', () => {
      it('should have a virtualserviceL4policysetstatsByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceL4policysetstatsByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceL4policysetstatsByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceL4policysetstatsByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceSescaleoutstatusByUuidGET - errors', () => {
      it('should have a virtualserviceSescaleoutstatusByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceSescaleoutstatusByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceSescaleoutstatusByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceSescaleoutstatusByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceScaleoutstatusByUuidGET - errors', () => {
      it('should have a virtualserviceScaleoutstatusByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceScaleoutstatusByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceScaleoutstatusByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceScaleoutstatusByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceScaleoutstatusDetailByUuidGET - errors', () => {
      it('should have a virtualserviceScaleoutstatusDetailByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceScaleoutstatusDetailByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceScaleoutstatusDetailByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceScaleoutstatusDetailByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#virtualserviceSsopolicystatsByUuidGET - errors', () => {
      it('should have a virtualserviceSsopolicystatsByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.virtualserviceSsopolicystatsByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.virtualserviceSsopolicystatsByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-virtualserviceSsopolicystatsByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ssopolicyGET - errors', () => {
      it('should have a ssopolicyGET function', (done) => {
        try {
          assert.equal(true, typeof a.ssopolicyGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ssopolicyPOST - errors', () => {
      it('should have a ssopolicyPOST function', (done) => {
        try {
          assert.equal(true, typeof a.ssopolicyPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.ssopolicyPOST(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-ssopolicyPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ssopolicyByUuidGET - errors', () => {
      it('should have a ssopolicyByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.ssopolicyByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.ssopolicyByUuidGET('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-ssopolicyByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ssopolicyByUuidPUT - errors', () => {
      it('should have a ssopolicyByUuidPUT function', (done) => {
        try {
          assert.equal(true, typeof a.ssopolicyByUuidPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.ssopolicyByUuidPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-ssopolicyByUuidPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.ssopolicyByUuidPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-ssopolicyByUuidPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ssopolicyByUuidPATCH - errors', () => {
      it('should have a ssopolicyByUuidPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.ssopolicyByUuidPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.ssopolicyByUuidPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-ssopolicyByUuidPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.ssopolicyByUuidPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-ssopolicyByUuidPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ssopolicyByUuidDELETE - errors', () => {
      it('should have a ssopolicyByUuidDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.ssopolicyByUuidDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.ssopolicyByUuidDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-ssopolicyByUuidDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#controllerpropertiesGET - errors', () => {
      it('should have a controllerpropertiesGET function', (done) => {
        try {
          assert.equal(true, typeof a.controllerpropertiesGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#controllerpropertiesPUT - errors', () => {
      it('should have a controllerpropertiesPUT function', (done) => {
        try {
          assert.equal(true, typeof a.controllerpropertiesPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.controllerpropertiesPUT('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-controllerpropertiesPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#controllerpropertiesPATCH - errors', () => {
      it('should have a controllerpropertiesPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.controllerpropertiesPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.controllerpropertiesPATCH('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-controllerpropertiesPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkGET - errors', () => {
      it('should have a networkGET function', (done) => {
        try {
          assert.equal(true, typeof a.networkGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkPOST - errors', () => {
      it('should have a networkPOST function', (done) => {
        try {
          assert.equal(true, typeof a.networkPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.networkPOST(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-networkPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkByUuidGET - errors', () => {
      it('should have a networkByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.networkByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.networkByUuidGET('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-networkByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkByUuidPUT - errors', () => {
      it('should have a networkByUuidPUT function', (done) => {
        try {
          assert.equal(true, typeof a.networkByUuidPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.networkByUuidPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-networkByUuidPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.networkByUuidPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-networkByUuidPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkByUuidPATCH - errors', () => {
      it('should have a networkByUuidPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.networkByUuidPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.networkByUuidPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-networkByUuidPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.networkByUuidPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-networkByUuidPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkByUuidDELETE - errors', () => {
      it('should have a networkByUuidDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.networkByUuidDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.networkByUuidDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-networkByUuidDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolGET - errors', () => {
      it('should have a poolGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolPOST - errors', () => {
      it('should have a poolPOST function', (done) => {
        try {
          assert.equal(true, typeof a.poolPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.poolPOST(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolByUuidGET - errors', () => {
      it('should have a poolByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolByUuidGET('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolByUuidPUT - errors', () => {
      it('should have a poolByUuidPUT function', (done) => {
        try {
          assert.equal(true, typeof a.poolByUuidPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolByUuidPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolByUuidPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.poolByUuidPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolByUuidPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolByUuidPATCH - errors', () => {
      it('should have a poolByUuidPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.poolByUuidPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolByUuidPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolByUuidPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.poolByUuidPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolByUuidPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolByUuidDELETE - errors', () => {
      it('should have a poolByUuidDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.poolByUuidDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolByUuidDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolByUuidDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolScaleoutByUuidPOST - errors', () => {
      it('should have a poolScaleoutByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.poolScaleoutByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolScaleoutByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolScaleoutByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.poolScaleoutByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolScaleoutByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolScaleinByUuidPOST - errors', () => {
      it('should have a poolScaleinByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.poolScaleinByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolScaleinByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolScaleinByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.poolScaleinByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolScaleinByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolClearByUuidPOST - errors', () => {
      it('should have a poolClearByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.poolClearByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolClearByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolClearByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.poolClearByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolClearByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeByUuidGET - errors', () => {
      it('should have a poolRuntimeByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolRuntimeByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolRuntimeByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolRuntimeByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeServerByUuidGET - errors', () => {
      it('should have a poolRuntimeServerByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolRuntimeServerByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolRuntimeServerByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolRuntimeServerByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeDetailByUuidGET - errors', () => {
      it('should have a poolRuntimeDetailByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolRuntimeDetailByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolRuntimeDetailByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolRuntimeDetailByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeServerDetailByUuidGET - errors', () => {
      it('should have a poolRuntimeServerDetailByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolRuntimeServerDetailByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolRuntimeServerDetailByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolRuntimeServerDetailByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeInternalByUuidGET - errors', () => {
      it('should have a poolRuntimeInternalByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolRuntimeInternalByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolRuntimeInternalByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolRuntimeInternalByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeServerInternalByUuidGET - errors', () => {
      it('should have a poolRuntimeServerInternalByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolRuntimeServerInternalByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolRuntimeServerInternalByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolRuntimeServerInternalByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeStatsClearByUuidPOST - errors', () => {
      it('should have a poolRuntimeStatsClearByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.poolRuntimeStatsClearByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolRuntimeStatsClearByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolRuntimeStatsClearByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.poolRuntimeStatsClearByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolRuntimeStatsClearByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeRequestQueueClearByUuidPOST - errors', () => {
      it('should have a poolRuntimeRequestQueueClearByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.poolRuntimeRequestQueueClearByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolRuntimeRequestQueueClearByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolRuntimeRequestQueueClearByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.poolRuntimeRequestQueueClearByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolRuntimeRequestQueueClearByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeDebugByUuidGET - errors', () => {
      it('should have a poolRuntimeDebugByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolRuntimeDebugByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolRuntimeDebugByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolRuntimeDebugByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolHmonByUuidGET - errors', () => {
      it('should have a poolHmonByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolHmonByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolHmonByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolHmonByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolRuntimeServerHmonstatByUuidGET - errors', () => {
      it('should have a poolRuntimeServerHmonstatByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolRuntimeServerHmonstatByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolRuntimeServerHmonstatByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolRuntimeServerHmonstatByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolAlgoByUuidGET - errors', () => {
      it('should have a poolAlgoByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolAlgoByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolAlgoByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolAlgoByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolPersistenceByUuidGET - errors', () => {
      it('should have a poolPersistenceByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolPersistenceByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolPersistenceByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolPersistenceByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolConnpoolByUuidGET - errors', () => {
      it('should have a poolConnpoolByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolConnpoolByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolConnpoolByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolConnpoolByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolConnpoolstatsClearByUuidPOST - errors', () => {
      it('should have a poolConnpoolstatsClearByUuidPOST function', (done) => {
        try {
          assert.equal(true, typeof a.poolConnpoolstatsClearByUuidPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolConnpoolstatsClearByUuidPOST(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolConnpoolstatsClearByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.poolConnpoolstatsClearByUuidPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolConnpoolstatsClearByUuidPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolHttpcacheByUuidGET - errors', () => {
      it('should have a poolHttpcacheByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolHttpcacheByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolHttpcacheByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolHttpcacheByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolHttpcachestatsByUuidGET - errors', () => {
      it('should have a poolHttpcachestatsByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolHttpcachestatsByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolHttpcachestatsByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolHttpcachestatsByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolHttpcachestatsDetailByUuidGET - errors', () => {
      it('should have a poolHttpcachestatsDetailByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolHttpcachestatsDetailByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolHttpcachestatsDetailByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolHttpcachestatsDetailByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolVsByUuidGET - errors', () => {
      it('should have a poolVsByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolVsByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolVsByUuidGET(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolVsByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolgroupGET - errors', () => {
      it('should have a poolgroupGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolgroupGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolgroupPOST - errors', () => {
      it('should have a poolgroupPOST function', (done) => {
        try {
          assert.equal(true, typeof a.poolgroupPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.poolgroupPOST(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolgroupPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolgroupByUuidGET - errors', () => {
      it('should have a poolgroupByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.poolgroupByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolgroupByUuidGET('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolgroupByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolgroupByUuidPUT - errors', () => {
      it('should have a poolgroupByUuidPUT function', (done) => {
        try {
          assert.equal(true, typeof a.poolgroupByUuidPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolgroupByUuidPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolgroupByUuidPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.poolgroupByUuidPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolgroupByUuidPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolgroupByUuidPATCH - errors', () => {
      it('should have a poolgroupByUuidPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.poolgroupByUuidPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolgroupByUuidPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolgroupByUuidPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.poolgroupByUuidPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolgroupByUuidPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#poolgroupByUuidDELETE - errors', () => {
      it('should have a poolgroupByUuidDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.poolgroupByUuidDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.poolgroupByUuidDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-poolgroupByUuidDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vsvipGET - errors', () => {
      it('should have a vsvipGET function', (done) => {
        try {
          assert.equal(true, typeof a.vsvipGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vsvipPOST - errors', () => {
      it('should have a vsvipPOST function', (done) => {
        try {
          assert.equal(true, typeof a.vsvipPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.vsvipPOST(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-vsvipPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vsvipByUuidGET - errors', () => {
      it('should have a vsvipByUuidGET function', (done) => {
        try {
          assert.equal(true, typeof a.vsvipByUuidGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.vsvipByUuidGET('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-vsvipByUuidGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vsvipByUuidPUT - errors', () => {
      it('should have a vsvipByUuidPUT function', (done) => {
        try {
          assert.equal(true, typeof a.vsvipByUuidPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.vsvipByUuidPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-vsvipByUuidPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.vsvipByUuidPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-vsvipByUuidPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vsvipByUuidPATCH - errors', () => {
      it('should have a vsvipByUuidPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.vsvipByUuidPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.vsvipByUuidPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-vsvipByUuidPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.vsvipByUuidPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-vsvipByUuidPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vsvipByUuidDELETE - errors', () => {
      it('should have a vsvipByUuidDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.vsvipByUuidDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.vsvipByUuidDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-avi_controller-adapter-vsvipByUuidDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
